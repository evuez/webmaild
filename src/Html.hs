module Html (span, html, p, hr, div_, iframe, img, main_, ul, li, table, td, th, tr, a) where

import Data.List (unwords)
import Intro

type Attr = (String, String)

html :: [String] -> String
html xs =
  concat
    [ "<html>"
    , "<head>"
    , script
        [ "function resizeIframe(x) { \
          \  x.style.height = x.contentWindow.document.documentElement.scrollHeight + 'px'; \
          \}"
        ]
    , "</head>"
    , "<body>"
    , style
        [ ":root { font-size: 16px; }"
        , "* { padding: 0; margin: 0; }"
        , "body { display: flex; flex-direction: column; gap: 1rem; padding: 1rem; }"
        , "hr { height: 0; border: 0; border-top: 1px solid #000; }"
        , "table { border-collapse: collapse; width: 100%; }"
        , "td, th { border: 1px solid #000; padding: 0.4rem; }"
        , "iframe { width: 100%; min-height: 80vh; border: none; }"
        , "main { display: flex; flex-direction: column; gap: 1rem; }"
        , ".part { background-color: #eee; }"
        , ".part-body { padding: 1rem; }"
        ]
    , concat xs
    , "</body></html>"
    ]

hr :: String
hr = "<hr>"

style :: [String] -> String
style xs = concat ["<style>", concat xs, "</style>"]

script :: [String] -> String
script xs = concat ["<script>", concat xs, "</script>"]

iframe :: [Attr] -> [String] -> String
iframe xs ys = concat ["<iframe onload='resizeIframe(this)' ", attrs xs, ">", concat ys, "</iframe>"]

div_ :: [Attr] -> [String] -> String
div_ xs ys = concat ["<div ", attrs xs, ">", concat ys, "</div>"]

main_ :: [String] -> String
main_ xs = concat ["<main>", concat xs, "</main>"]

p :: [String] -> String
p xs = concat ["<p>", concat xs, "</p>"]

span :: [String] -> String
span xs = concat ["<span>", concat xs, "</span>"]

a :: [Attr] -> [String] -> String
a xs ys = concat ["<a ", attrs xs, ">", concat ys, "</a>"]

table :: [String] -> String
table xs = concat ["<table>", concat xs, "</table>"]

td :: [String] -> String
td xs = concat ["<td>", concat xs, "</td>"]

th :: [String] -> String
th xs = concat ["<th>", concat xs, "</th>"]

tr :: [String] -> String
tr xs = concat ["<tr>", concat xs, "</tr>"]

ul :: [String] -> String
ul xs = concat ["<ul>", concat xs, "</ul>"]

li :: [String] -> String
li xs = concat ["<li>", concat xs, "</li>"]

img :: [Attr] -> String
img xs = concat ["<img ", attrs xs, ">"]

attr :: Attr -> String
attr (k, v) = concat [k, "='", v, "'"]

attrs :: [Attr] -> String
attrs xs = unwords (attr <$> xs)
