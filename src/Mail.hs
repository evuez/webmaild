module Mail
  ( Mail (..)
  , subject
  , Header
  , P.Part (..)
  , MailM
  , newMail
  , setData
  )
where

import Common (toLower)
import Control.Monad.State.Lazy (StateT)
import Data.List (find)
import Intro
import Mail.Header (Header)
import qualified Mail.Parser as P

data Mail = Mail
  { client :: String
  , from :: String
  , to :: [String]
  , headers :: [Header]
  , body :: [P.Part]
  }
  deriving (Show, Eq)

type MailM a = StateT Mail IO a

newMail :: Mail
newMail = Mail "" "" [] [] []

setData :: [String] -> Mail -> Mail
setData xs m = m{headers = msg.headers, body = msg.body}
 where
  msg = P.run xs

subject :: Mail -> Maybe String
subject m = snd <$> find (\(k, _) -> toLower k == "subject") (headers m)
