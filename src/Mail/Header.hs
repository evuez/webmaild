module Mail.Header
  ( ContentType (..)
  , Header
  , contentType
  , contentTransferEncoding
  )
where

import Common (findVal, split2, splitOn, toLower, trim, trimWith)
import Intro

type Header = (String, String)

data ContentType = ContentType
  { mime :: String
  , boundary :: Maybe String
  , charset :: Maybe String
  }
  deriving (Show)

newContentType :: String -> ContentType
newContentType m = ContentType{mime = m, boundary = Nothing, charset = Nothing}

contentType :: [Header] -> Maybe ContentType
contentType xs = do
  v <- findValIns "content-type" xs
  case (fmap trim . splitOn (== ';')) v of
    [y] -> pure (newContentType y)
    y : ys ->
      let kv = fmap (split2 (== '=')) ys
       in pure
            $ (newContentType y)
              { boundary = trimQuotes <$> findValIns "boundary" kv
              , charset = findValIns "charset" kv
              }
    [] -> Nothing

contentTransferEncoding :: [Header] -> Maybe String
contentTransferEncoding = findValIns "content-transfer-encoding"

findValIns :: String -> [Header] -> Maybe String
findValIns k = findVal ((== k) . toLower)

trimQuotes :: String -> String
trimQuotes = trimWith (\x -> x == '"' || x == ' ')
