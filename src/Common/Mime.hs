module Common.Mime (getType, getSubtype, MimeType (..)) where

import Common (split2, trim)
import Intro

data MimeType = Text String | Image String | Application String

getType :: String -> Maybe MimeType
getType mime = case type_ of
  "text" -> Just (Text subtype)
  "image" -> Just (Image subtype)
  "application" -> Just (Application subtype)
  _ -> Nothing
 where
  (type_, subtype) = split2 (== '/') $ trim mime

getSubtype :: String -> String
getSubtype = trim . snd . split2 (== '/')

instance Show MimeType where
  show (Text sub) = "text/" ++ sub
  show (Image sub) = "image/" ++ sub
  show (Application sub) = "application/" ++ sub
