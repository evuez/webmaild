.PHONY: format
format:
	fourmolu -i app/
	fourmolu -i src/

.PHONY: lint
lint:
	find {app,src}/ | entr -c hlint src/ app/

.PHONY: run
run:
	find {app,src}/ | entr -cr cabal new-run

.PHONY: repl
repl:
	ghcid --allow-eval --lint --command "cabal repl"
