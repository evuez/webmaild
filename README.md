# webmaild

A small SMTP server with a web interface to visualize received emails.

    webmaild -smtp-port 5678 -http-port 8765

will start an SMTP and HTTP server locally, ready to accept emails. Go to http://localhost:8765 to view the emails received.

No authentication is required, but it will happily accept any authentication method and ignore it.

The default ports, if none are provided, are `5879` for SMTP and `9785` for HTTP.

Note that this is based on very loose interpretations of the respective RFCs for SMTP, HTTP and emails, and is only intended to help with debugging. I tested with like, 2 emails, and it worked well enough (:

## Building & Running

The recommended solution is to use `nix`:

- Use `nix profile install https://git.k.mulga.net/julien/webmaild/snapshot/webmaild-main.tar.gz` to install webmaild,
- Use `nix run https://git.k.mulga.net/julien/webmaild/snapshot/webmaild-main.tar.gz` to build and run the binary directly.

If you have `cabal` installed, use `cabal build` or `cabal install`.
